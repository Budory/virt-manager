From f44c6ec970413843214f52d5523ee8cf277b0150 Mon Sep 17 00:00:00 2001
From: Andrea Bolognani <abologna@redhat.com>
Date: Mon, 12 Dec 2022 19:38:22 +0100
Subject: [PATCH] virt-install: Document Secure Boot setups

Provide ready to use recipes for explicitly enabling and
explicitly disabling Secure Boot, as well as a pointer to
the more extensive information found on the libvirt website.

Setting loader_secure=yes is only one part of a proper Secure
Boot setup, so stop documenting it in the section about manual
firmware selection to avoid confusion.

https://bugzilla.redhat.com/show_bug.cgi?id=2112154
https://bugzilla.redhat.com/show_bug.cgi?id=2149971

Signed-off-by: Andrea Bolognani <abologna@redhat.com>
(cherry picked from commit 33ff193ee9fcfdb74f95d946a1b93239a1a12a61)

Resolves: https://bugzilla.redhat.com/show_bug.cgi?id=2112154

Signed-off-by: Jonathon Jongsma <jjongsma@redhat.com>
---
 man/virt-install.rst | 17 +++++++++++++++--
 1 file changed, 15 insertions(+), 2 deletions(-)

diff --git a/man/virt-install.rst b/man/virt-install.rst
index 684f22655..a0df73280 100644
--- a/man/virt-install.rst
+++ b/man/virt-install.rst
@@ -957,13 +957,26 @@ Some examples:
     via domcapabilities XML, so this will likely only work if using properly
     configured distro packages. This is the recommended UEFI setup.
 
+``--boot uefi,firmware.feature0.name=secure-boot,firmware.feature0.enabled=yes,firmware.feature1.name=enrolled-keys,firmware.feature1.enabled=yes``
+    Configure the VM to boot from UEFI with Secure Boot support enabled.
+    Only signed operating systems will be able to boot with this configuration.
+
+``--boot uefi,firmware.feature0.name=secure-boot,firmware.feature0.enabled=no``
+    Configure the VM to boot from UEFI with Secure Boot support disabled.
+    This configuration allows both signed and unsigned operating systems to
+    run.
+
+    Additional information about the ``secure-boot`` and
+    ``enrolled-keys`` firmware features and how they can be used to
+    influence firmware selection is available at
+    https://libvirt.org/kbase/secureboot.html
+
 ``--boot loader=/.../OVMF_CODE.fd,loader.readonly=yes,loader.type=pflash,nvram.template=/.../OVMF_VARS.fd,loader_secure=no``
     Specify that the virtual machine use the custom OVMF binary as boot firmware,
     mapped as a virtual flash chip. In addition, request that libvirt instantiate
     the VM-specific UEFI varstore from the custom "/.../OVMF_VARS.fd" varstore
     template. This setup is not recommended, and should only be used if
-    --boot uefi doesn't know about your UEFI binaries. If your UEFI firmware
-    supports Secure boot feature you can enable it via loader_secure.
+    --boot uefi doesn't know about your UEFI binaries.
 
 Use --boot=? to see a list of all available sub options.
 Complete details at https://libvirt.org/formatdomain.html#elementsOS
-- 
2.39.1

